FROM debian:bullseye-slim

RUN apt update
RUN apt install -y git-core
RUN git config --global user.email "gitlab@ci.com"
RUN git config --global user.name "Gitlab CI"
